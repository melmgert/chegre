# Chegre - Facing beauty.

![Chegre Logo](logo.png "Logo")

Chegre (from Check + Hegre) is a utility for checking the daily update at hegre.com in a neat manner without the need to visit the actual website. Chegre displays the title of the update as well as the portaits of the models featured. *Face.js* is used to automatically detect the faces of the featured models, while the rest of the portrait is being pixelised. All done inside the browser.

### Why?

I don't know, honestly. Boredom. Maybe.

### How?

Due to CORS restrictions, when loading cross-origin resources in the browser, a public proxy service, namely [allOrigins](https://allorigins.win), is used to fetch the necessary data (*basically rudimentary webscraping*). The portraits of the models are then drawn to a canvas, in order to pixelise them by averaging the colour of *n x n*-blocks, while [Face.js](https://github.com/justadudewhohacks/face-api.js/) is used in the background to detect the area containing the faces. If no face is detected (some of the models choose to stay *anonymous*), the image is just left entirely pixelised. Otherwise the face is overlayed unpixelised onto the pixelised canvas in order to only reveal the face of the model.